# COMP2300 lab 1 template

<https://cs.anu.edu.au/courses/comp2300/labs/01-intro/>

This is the lab 1 template for writing pure-asm code programs for your
discoboard comp2300. It assumes you're using VSCode and have set things up
according to the [software setup
instructions](https://cs.anu.edu.au/courses/comp2300/resources/software-setup/).

Open the root folder, start writing code in `src/main.S` and off you go!

If you have any questions, ask them on
[Piazza](https://piazza.com/anu.edu.au/spring2018/comp23006300/home).
